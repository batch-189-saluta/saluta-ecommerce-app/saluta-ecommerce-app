

export default function OrderCard ({orderProp}) {

	console.log(orderProp)

	return (

			<Table striped bordered hover>
		      <thead>
		        <tr>
		          <th>Item Number</th>
		          <th>Product Name</th>
		          <th>Unit Cost</th>
		          <th>Total Amount</th>
		          <th>Status</th>
		        </tr>
		      </thead>
		      <tbody>
		        <tr>
		          <td>1</td>
		          <td>Mark</td>
		          <td>Otto</td>
		          <td>@mdo</td>
		          <td>
		          	<Button>Cancel</Button>
		          </td>
		        </tr>
		      </tbody>
		    </Table>

		)

}