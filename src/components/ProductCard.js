// import { useState, useEffect } from 'react'
import { Card, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import './ProductCard.css'

export default function ProductCard({productProp}) {


	// Deconstruct the course properties into their own variables
	const {name, description, price, _id} = productProp;
	// console.log(productProp)

	return (
			<div className="row m-4 justify-content-center">
				<Card className="productCard mb-4 col-md-8 col-lg-6">
			      <Card.Body>
			        <Card.Title className="productCard-title text-center pb-3"><strong>{name}</strong></Card.Title>
			    {/*pwede din gamitin card.Title, card.Subtitle, card.Text*/}
			    	<Card.Subtitle className="productCard-title"><strong>Description:</strong></Card.Subtitle>
			        <Card.Text>{description}</Card.Text>
			        <Card.Subtitle className="productCard-title"><strong>Price:</strong></Card.Subtitle>
			        <Card.Text>Php {price}</Card.Text>
			        <div className="row justify-content-end">
			        	<Button className="button" as={Link} to={`/products/${_id}`}>Buy</Button>
			        </div>
			        
			      </Card.Body>
			    </Card>
		    </div>
			
		)
}

