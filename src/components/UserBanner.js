import {useContext} from 'react'
import {Row, Col, Button} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import UserContext from '../UserContext'
import './Banner.css'

export default function Banner () {

	const {user} = useContext(UserContext)
	console.log(user)

	return (

			<Row>
				{/*className is a property*/}
				<Col id="banner" className="p-5 md-6">
					<div className="banner-title d-inline-block p-3">
						<h1 >Luna  Quarter</h1>
						<h2>Builders Inc.</h2>
					</div><br/><br/>
					
					<p className="banner-title d-inline-block p-2">Welcome Back, <strong>{user.firstName}</strong>!</p>
				</Col>
			</Row>
		)
}