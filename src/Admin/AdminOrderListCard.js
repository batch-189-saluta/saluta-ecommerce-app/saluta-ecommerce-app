import {useContext} from 'react'
import {Card, Button} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import UserContext from	'../UserContext'
import './AdminOrderListCard.css'

export default function AdminOrderListCard({adminProductProp}) {

	const {userId, quantity, totalCost, purchasedOn} = adminProductProp
	// console.log(adminProductProp)

	


	return (

			<>
				<div className="card row justify-content-center">
					<div className="m-2 p-3 col-md-8 col-lg-6">
						{/*<h6> <strong>Name:</strong> {name}</h6>*/}
						<h6> <strong>userId:</strong> {userId}</h6>
						<h6> <strong>Quantity:</strong> {quantity}</h6>
						<h6> <strong>Total Amount:</strong> {totalCost}</h6>
						<h6> <strong>Date:</strong> {purchasedOn}</h6>
					</div>
				</div>
			</>
		)
}