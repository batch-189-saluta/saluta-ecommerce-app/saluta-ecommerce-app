import {useState, useEffect, useContext} from 'react'
import {Form, Button} from 'react-bootstrap'
import {Navigate, useNavigate, Link} from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'
// import './AddProduct.css'

export default function AddProduct () {

	// for authentication------------------------------------
	const {user} = useContext(UserContext)

	// for redirection after adding a product------------------
	const navigate = useNavigate()


	// state hooks to store values of input fields-----------------------------
	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState("")

	// enabling of button---------------------------------------
	const [isActive, setIsActive] = useState(false)

	useEffect(() => {
		if(name !== "" && description !== "" && price !== ""){

			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [name, description, price])



	// submit function------------------------------------------

	function registerProduct (e) {

		// console.log(localStorage.getItem('token'))

		e.preventDefault()

		fetch('https://floating-atoll-72456.herokuapp.com/api/products/register', {

			method: 'POST',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({

				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {

			// console.log(data)s
			// dapat true

			if(data === true) {
				Swal.fire({
					title: "Product Registration Complete!",
					icon: "success",
					text: "Magpayaman ka na pre!"
				})

				setName("")
				setDescription("")
				setPrice("")

				navigate("/adminProducts")

			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Wag mo sisihin sarili mo. Please try again"
				})
			}
		})
	}








	return (

			(user.id !== null) ?
				(user.isAdmin == true) ?
					<div className="row justify-content-center">
					<Form className="m-4 col-md-6" onSubmit={(e) => registerProduct(e)}>
						<h1 className="text-center m-4 mb-5"><strong>Register a Product</strong></h1>

					{/*Product Name input-----------------------------------------*/}
				      <Form.Group className="mb-3" controlId="productName">
				        <Form.Label>Product Name</Form.Label>
				        <Form.Control 
				        	type="text" 
				        	placeholder="Enter Product Name"
				        	value={name}
				        	onChange={e => {
				        		setName(e.target.value)
				        	}}
				        	required />
				      </Form.Group>

				    {/*Description input-----------------------------------------*/}
				      <Form.Group className="mb-3" controlId="description">
				        <Form.Label>Description</Form.Label>
				        <Form.Control 
				        	as="textarea"
				        	rows="3"
				        	placeholder="Enter Description"
				        	value={description}
				        	onChange={e => {
				        		setDescription(e.target.value)
				        	}}
				        	required />
				      </Form.Group>

				  {/*Price input-----------------------------------------*/}
				      <Form.Group className="mb-3" controlId="price">
				        <Form.Label>Price</Form.Label>
				        <Form.Control 
				        	type="number" 
				        	placeholder="Enter Price"
				        	value={price}
				        	onChange={e => {
				        		setPrice(e.target.value)
				        	}}
				        	required />
				      </Form.Group>

					

				      {
				      	isActive ?
				      		<Button  type="submit" id="submitBtn" className="button1 m-1">
						        Submit
						      </Button>
				      :
				      		<Button  type="submit" id="submitBtn" className="button2 m-1" disabled>
						        Submit
						      </Button>
				      }
				      <Button variant="danger" type="submit" id="submitBtn" as={Link} to="/adminProducts" className="m-1">
						        Cancel
						      </Button>
				    </Form>
				    </div>

				:
					<Navigate to="*" />
			:
				<Navigate to="*" />
				
		)
}